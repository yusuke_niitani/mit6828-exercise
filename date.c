#include "types.h"
#include "user.h"
#include "date.h"

int
main(int argc, char *argv[])
{
  struct rtcdate r;
  // (+) dont know how to return 1 from syscall
  /*if (date(&r)) {
    printf(2, "date failed\n");
    exit();
  }
  */
  // your code to print the time in any format you like...
  date(&r);
  printf(1, "year: "); //(+) this printf function is never called..  not to mention below one
  //printf(1, "year:%d month:%d date:%d hour:%d minute:%d second:%d\n", r.year, r.month, r.day, r.hour, r.minute, r.second);
  
  exit();
}

